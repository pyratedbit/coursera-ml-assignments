**SOLUTIONS TO ML COURSE PROGRAMMING ASSIGNMENTS**

These are my solutions to the programming assignments from the course 'Machine Learning' on Coursera by Stanford University and taught by Andrew Ng.


**OVERVIEW OF THE EXERCISES**

1. Exercise 1


	Implementation of **Univariate and Multivariate Linear Regression** and Visualization of Cost Function
	
2. Exercise 2

	Implementation of **Logistic Regression** (with and without Regularization) and Plotting the Decision Boundaries.
	
3. Exercise 3

	Implementation of **One-vs-All Logistic Regression and Neural Networks** for Recognition of Hand-Writtend Digits
	
4. Exercise 4

	Implementation of **Backpropagation Algorithm** for Neural Networks 
	
5. Exercise 5

	Implementation of **Regularized Linear Regression** and Using it for Study of Models with Different Bias-Variance Properties.
	
6. Exercise 6

	Implementation of **Support Vector Machines(SVMs)** to Build A Spam Classifier.
	
7. Exercise 7

	Implementation of **K-means Algorithm** for Unsupervised Learning and Application for Image Compression and Implementation of **Principal Component Analysis(PCA)** to find a Low Dimensional Representation of Face Images.
	

	
	